{ lib
, openssl
, stdenv
, pkg-config
, rustPlatform
, fetchCrate
, makeWrapper
, wasm-pack
, CoreServices
}:

rustPlatform.buildRustPackage rec {
  pname = "perseus-cli";
  version = "0.4.0-beta.11";

  src = fetchCrate {
    inherit pname version;
    sha256 = "sha256-/MfnV2avPJaVmMJ+iPRSAKijYyu6X60sDIoCjDs7PRY=";
  };

  cargoSha256 = "sha256-b3JNo7FelH95gj4jREsJCJumyVq7YF9xjk3OD3QMSzQ=";

  nativeBuildInputs = [ makeWrapper pkg-config ];
  buildInputs = [ openssl ] ++ lib.optionals stdenv.isDarwin [ CoreServices ];

  postInstall = ''
    wrapProgram $out/bin/perseus \
      --prefix PATH : "${lib.makeBinPath [ wasm-pack ]}"
  '';

  meta = with lib; {
    homepage = "https://arctic-hen7.github.io/perseus";
    description = "A high-level web development framework for Rust with full support for server-side rendering and static generation";
    maintainers = with maintainers; [ max-niederman ];
    license = with licenses; [ mit ];
    mainProgram = "perseus";
  };
}
