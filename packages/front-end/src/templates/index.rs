#[perseus::browser]
use gloo::utils::window;
use {
	crate::{
		hooks::net::{use_web_socket, WebSocketHandle},
		macros::if_browser,
	},
	hex::FromHex,
	perseus::{spawn_local_scoped, Template},
	serde::{Deserialize, Serialize},
	std::{fmt::Display, str::FromStr},
	sycamore::{
		component,
		prelude::{view, Html, Indexed, Keyed, Scope, SsrNode, View},
		reactive::{create_effect, create_signal, Signal},
		Prop,
	},
	sycamore_hooks::net::Message,
};

#[derive(Serialize, Deserialize)]
struct Colour {
	red: u8,
	green: u8,
	blue: u8,
}
impl FromStr for Colour {
	type Err = hex::FromHexError;

	fn from_str(s: &str) -> Result<Self, Self::Err> {
		let [red, green, blue] = <[u8; 3]>::from_hex(s)?;

		Ok(Self { red, green, blue })
	}
}
impl Display for Colour {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		write!(f, "#{:02X}{:02X}{:02X}", self.red, self.green, self.blue)
	}
}

#[derive(Serialize)]
enum ClientCommand {
	GetLedColour,
	SetLedColour(Colour),
	GetTemperature,
	GetRelativeHumidity,
	SetLedBrightness(u8),
	SetPumpSpeed(u8),
	SetLedMode(LedMode),
}

// #[allow(clippy(enum_variant_names))]
#[derive(Deserialize)]
enum ServerResponse {
	GetLedColour(Colour),
	GetTemperature(u8),
	GetRelativeHumidity(u8),
}

#[derive(Serialize, Deserialize, Clone, Copy, PartialEq, Eq)]
enum LedMode {
	SingleColourRainbow,
	PartyHard,
	SpinningRainbow,
}
impl LedMode {
	fn to_string_id(self) -> &'static str {
		match self {
			LedMode::SingleColourRainbow => "single_colour_rainbow",
			LedMode::PartyHard => "party_hard",
			LedMode::SpinningRainbow => "spinning_rainbow",
		}
	}

	fn from_string_id(id: &str) -> Option<Self> {
		match id {
			"single_colour_rainbow" => Some(LedMode::SingleColourRainbow),
			"party_hard" => Some(LedMode::PartyHard),
			"spinning_rainbow" => Some(LedMode::SpinningRainbow),
			_ => None,
		}
	}
}

#[perseus::template_rx]
pub fn index_page<G: Html>(cx: Scope) -> View<G> {
	let colour = create_signal(cx, "#ffffff".to_string());
	let led_mode = create_signal(cx, LedMode::SingleColourRainbow.to_string_id().to_string());
	let pump_speed = create_signal(cx, "0".to_string());
	let show_editor = create_signal(cx, false);

	let websocket_address = if_browser!({format!("ws://{}/ws", window().location().host().expect("reading website host"))} else {"".to_string()});

	let ws = use_web_socket(cx, &websocket_address).expect("could not connect to websocket");

	create_effect(cx, move || {
		#[perseus::browser]
		spawn_local_scoped(cx, async move {
			ws.send(Message::Bytes(
				postcard::to_allocvec(&ClientCommand::GetLedColour).unwrap(),
			))
			.await
			.expect("could not send message");
			let Some(res) = ws.read_binary().await else { return };
			let ServerResponse::GetLedColour(res_colour) = postcard::from_bytes::<'_, ServerResponse>(
				&res.expect("couldn't read server response"),
			)
			.expect("couldn't recode server response") else { panic!("server responded to different request type!") };

			colour.set(res_colour.to_string());

			show_editor.set(true);
		});
	});

	view! { cx,
		style {
			"""
				button {
					margin-bottom: 2rem;
				}
				input, select, button {
					width: 6rem;
				}
			"""
		}

		// Don't worry, there are much better ways of styling in Perseus!
		div(style = "display: flex; flex-direction: column; justify-content: center; align-items: center; height: 95vh;") {
			h1 { "Welcome to FrantaAjoutíProjekt!" }

			(if *show_editor.get() { view! {cx,
				Editor(colour = colour, led_mode = led_mode, pump_speed = pump_speed, ws = ws)
			} } else { view! {cx,} })

			p(style = format!("background-color: {};", colour.get())) {
				"This is just an example app. Try changing some code inside "
			}
		}
	}
}

fn led_modes_with_labels() -> Vec<(LedMode, &'static str)> {
	vec![
		(LedMode::SingleColourRainbow, "single colour"),
		(LedMode::PartyHard, "party hard"),
		(LedMode::SpinningRainbow, "spinning rainbow"),
	]
}

#[derive(Prop)]
struct EditorProps<'a> {
	colour: &'a Signal<String>,
	led_mode: &'a Signal<String>,
	pump_speed: &'a Signal<String>,
	ws: WebSocketHandle<'a>,
}
#[component]
fn Editor<'a, G: Html>(
	cx: Scope<'a>,
	EditorProps {
		colour,
		led_mode,
		pump_speed,
		ws,
	}: EditorProps<'a>,
) -> View<G> {
	let led_modes_with_labels = create_signal(cx, led_modes_with_labels());

	view! { cx,
		input (type = "color", bind:value = colour)

		button(type="button", on:click=move |_| {
			spawn_local_scoped(cx, async move {
				let colour = colour.get().as_ref()[1..].parse().unwrap();

				ws.send(Message::Bytes(postcard::to_allocvec(
					&ClientCommand::SetLedColour(colour)).unwrap()
				))
				.await
				.expect("could not send message");
			});
		}) { "Set colour" }

		input (type = "number", min = 0, max = 255, bind:value = pump_speed)

		button(type="button", on:click=move |_| {
			spawn_local_scoped(cx, async move {
				let speed = pump_speed.get().as_ref().parse().unwrap();

				ws.send(Message::Bytes(postcard::to_allocvec(
					&ClientCommand::SetPumpSpeed(speed)).unwrap()
				))
				.await
				.expect("could not send message");
			});
		}) { "Set speed" }

		select(bind:value = led_mode) {
			Indexed(
				iterable = led_modes_with_labels,
				view=|cx, (value, label)| view! { cx,
					option(value = value.to_string_id()) { (label) }
				},
			)
		}

		button(type="button", on:click=move |_| {
			spawn_local_scoped(cx, async move {
				let speed = LedMode::from_string_id(led_mode.get().as_ref()).expect("encountered invalid led mode id");

				ws.send(Message::Bytes(postcard::to_allocvec(
					&ClientCommand::SetLedMode(speed)).unwrap()
				))
				.await
				.expect("could not send message");
			});
		}) { "Set mode" }
	}
}

#[perseus::head]
pub fn head(cx: Scope) -> View<SsrNode> {
	view! { cx,
		title { "Welcome to Perseus!" }
	}
}

pub fn get_template<G: Html>() -> Template<G> {
	Template::new("index").template(index_page).head(head)
}
