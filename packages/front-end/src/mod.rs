#![feature(cfg_target_compact)]
#![feature(decl_macro)]
#![feature(proc_macro_hygiene)]

mod error_pages;
mod hooks;
mod macros;
mod templates;

use perseus::{Html, PerseusApp};

#[perseus::main(perseus_warp::dflt_server)]
pub fn main<G: Html>() -> PerseusApp<G> {
	#[cfg(target(arch = "wasm32"))]
	console_error_panic_hook::set_once();
	PerseusApp::new()
		.template(crate::templates::index::get_template)
		.error_pages(crate::error_pages::get_error_pages)
}
