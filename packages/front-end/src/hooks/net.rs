#![allow(unused_variables)]

pub use gloo::net::websocket::Message;
use {
	crate::macros::if_browser,
	gloo::{
		net::{websocket, websocket::WebSocketError},
		utils::errors::JsError,
	},
	sycamore::prelude::*,
};
#[cfg(target(arch = "wasm32"))]
use {
	futures::{SinkExt, StreamExt},
	gloo::net::websocket::futures::WebSocket,
	std::cell::RefCell,
	sycamore::futures::spawn_local_scoped,
};

#[derive(Clone, Copy)]
pub struct WebSocketHandle<'a> {
	#[cfg(target(arch = "wasm32"))]
	ws: &'a RefCell<WebSocket>,
	state: &'a Signal<websocket::State>,
	message: &'a Signal<String>,
	message_bytes: &'a Signal<Vec<u8>>,
}

/// Opens a web socket connection at the specified `url`. The connection is closed when the enclosing scope is destroyed
/// or when [`WebSocketHandle::close`] is called.
pub fn use_web_socket<'a>(cx: Scope<'a>, url: &str) -> Result<WebSocketHandle<'a>, JsError> {
	let state = create_signal(cx, websocket::State::Closed);
	let message = create_signal(cx, String::new());
	let message_bytes = create_signal(cx, Vec::new());

	#[perseus::browser]
	let ws = WebSocket::open(url)?;

	Ok(WebSocketHandle {
		#[cfg(target(arch = "wasm32"))]
		ws: create_ref(cx, RefCell::new(ws)),
		state,
		message,
		message_bytes,
	})
}

impl<'a> WebSocketHandle<'a> {
	pub async fn send(self, message: Message) -> Result<(), WebSocketError> {
		#[cfg(not(target(arch = "wasm32")))]
		panic!("can't send WS messages outside of browser!");
		#[cfg(target(arch = "wasm32"))]
		self.ws.borrow_mut().send(message).await
	}

	pub async fn read(self) -> Option<Result<Message, WebSocketError>> {
		if_browser!({ self.ws.borrow_mut().next().await } else { None })
	}

	pub async fn read_binary(self) -> Option<Result<Vec<u8>, WebSocketError>> {
		self.read().await.map(|r| {
			r.and_then(|m| match m {
				Message::Text(t) => Err(WebSocketError::ConnectionError),
				Message::Bytes(b) => Ok(b),
			})
		})
	}

	pub fn state(self) -> &'a ReadSignal<websocket::State> {
		self.state
	}

	/// NOTE: Not yet implemented due to technical reasons.
	pub fn close(self) {
		unimplemented!();
	}
}
