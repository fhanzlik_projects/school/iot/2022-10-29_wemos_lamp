pub macro if_browser {
	($browser:tt) => {
		if_browser! { { $browser } else () }
	},
	({ $browser:expr } else { $server:expr }) => {{
		let result;
		#[cfg(target(arch = "wasm32"))]
		{ result = $browser; }
		#[cfg(not(target(arch = "wasm32")))]
		{ result = $server; }
		result
	}}
}
