#define DOCTEST_CONFIG_IMPLEMENT  // REQUIRED: Enable custom main()
#include <doctest.h>
#include <bitset>
#include <optional>

#include "../src/postcard.hpp"
#include "../src/api_models.hpp"

using std::vector;
using std::optional;
using std::nullopt;
using api_models::ClientCommand;
namespace client_command = api_models::client_command;

template <typename T> class Bin {
	public:
		T value;
		explicit Bin(T value) : value(value) {}

		friend bool operator==(const Bin<T>& lhs, const Bin<T>& rhs) {
			return lhs.value == rhs.value;
		}
		template <typename U> friend bool operator==(const Bin<T>& lhs, const U& rhs) {
			return lhs.value == rhs;
		}
		friend std::ostream& operator<< (std::ostream& os, const Bin<T>& container) {
			os << "0b";
			for (uint8_t i = sizeof(T); i--; ) {
				os << std::bitset<8>((container.value >> i * 8) & 0xff).to_string();
				if (i != 0) {
					os << '\'';
				}
			}
			return os;
		}
};

TEST_CASE("decoding simple bytes") {
	auto buffer = vector<uint8_t> { 0x01, 0xff, 0x03, 0xa5 };
	auto parser = postcard::Parser(buffer.begin(), buffer.end());

	SUBCASE("read single") {
		CHECK(parser.eatU8() == 0x01);
	}
	SUBCASE("read multiple") {
		CHECK(parser.eatU8() == 0x01);
		CHECK(parser.eatU8() == 0xff);
		CHECK(parser.eatU8() == 0x03);
		CHECK(parser.eatU8() == 0xa5);
	}
	SUBCASE("read past end") {
		CHECK(parser.eatU8() == 0x01);
		CHECK(parser.eatU8() == 0xff);
		CHECK(parser.eatU8() == 0x03);
		CHECK(parser.eatU8() == 0xa5);
		CHECK(parser.eatU8() == nullopt);
	}
}

void testEatSingleVarLenU32(vector<uint8_t> buffer, uint32_t expectedResult) {
	auto parser = postcard::Parser(buffer.begin(), buffer.end());
	CHECK(parser.eatVarLenU32() == expectedResult);
	CHECK(parser.eatU8() == nullopt);
}
TEST_CASE("decoding variable-width ints") {
	SUBCASE("single byte") {
		testEatSingleVarLenU32(vector<uint8_t> { 0b01000111 }, 0b01000111);
	}
	SUBCASE("two bytes") {
		testEatSingleVarLenU32(vector<uint8_t> { 0xFF, 0x7F }, 0x3F'FF);
	}
	SUBCASE("three bytes") {
		testEatSingleVarLenU32(vector<uint8_t> { 0x81, 0x80, 0x01 }, 0x40'01);
	}
}

ClientCommand parseCommandBuffer(vector<uint8_t> buffer) {
	auto result = postcard::parseClientCommand(buffer.begin(), buffer.end());
	CHECK(result.has_value());
	return result.value();
}
TEST_CASE("decoding commands") {
	SUBCASE("read valid GetColour") {
		auto command = parseCommandBuffer({ 0x00 });
		CHECK(std::holds_alternative<client_command::GetColour>(command));
	}
	SUBCASE("read valid SetColour") {
		auto command = parseCommandBuffer({ 0x01, 0xff, 0x00, 0xa5 });
		CHECK(std::holds_alternative<client_command::SetColour>(command));
		auto colour = std::get<client_command::SetColour>(command).colour;
		CHECK(colour.red   == 0xff);
		CHECK(colour.green == 0x00);
		CHECK(colour.blue  == 0xa5);
	}
}

int main(int argc, char **argv) {
	doctest::Context context;

	// BEGIN:: PLATFORMIO REQUIRED OPTIONS
	context.setOption("success", true);     // Report successful tests
	context.setOption("no-exitcode", true); // Do not return non-zero code on failed test case
	// END:: PLATFORMIO REQUIRED OPTIONS

	context.applyCommandLine(argc, argv);
	return context.run();
}
