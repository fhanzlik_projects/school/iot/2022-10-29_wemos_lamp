#include <Arduino.h>
#include <Adafruit_GFX.h>
#include <Adafruit_NeoPixel.h>
#include <Adafruit_ST7735.h>
#include <AsyncElegantOTA.h>
#include <ESP8266WiFi.h>
#include <FS.h>
#include <iterator>
#include <LittleFS.h>
#include <qrcodegen.h>

#include "api_models.hpp"
#include "postcard.hpp"
#include "drivers.hpp"
#include "iterators.hpp"

namespace client_command = api_models::client_command;
using api_models::ClientCommand;
using api_models::Colour;
using api_models::LedMode;

// | D	0 | RX
// | D	1 | TX
// | D	2 | display CS
// | D	3 | motor A pwm (pump)
// | D	4 | display A0 / DC
// | D	5 | SCK
// | D	6 | display reset
// | D	7 | SDA
// | D  8 | display LED
// | D  9 | DHT11
// | D 10 | led strip
// | D 11 | motor B pwm (unused)
// | D 12 | motor A dir (pump)
// | D 13 | motor B dir (unused)

const uint16_t led_strip_led_count = 18;

auto pump      = Motor(Motor::Pins { dir: D7, pwm: D1 });
auto dht       = Dht(D2, Dht::Type::Dht11);
auto led_strip = Adafruit_NeoPixel(led_strip_led_count, D3, NEO_BRG + NEO_KHZ400);

auto server           = AsyncWebServer(80);
auto webSocketHandler = AsyncWebSocket("/ws");
auto serverEvents     = AsyncEventSource("/events");

enum DisplayColor {
	Black   = ST7735_BLACK,
	Blue    = ST7735_BLUE,
	Red     = ST7735_RED,
	Green   = ST7735_GREEN,
	Cyan    = ST7735_CYAN,
	Magenta = ST7735_MAGENTA,
	Yellow  = ST7735_YELLOW,
	White   = ST7735_WHITE,
};

volatile auto colour = Colour { .red = 255, .green = 0, .blue = 255 };
volatile auto ledMode = LedMode::SingleColourRainbow;

uint8_t saturatingAddU8(uint8_t a, uint8_t b) {
	uint8_t c = a + b;

	// can only happen due to overflow
	if (c < a) {
		c = -1;
	}
	return c;
}

uint16_t saturatingAddU16(uint16_t a, uint16_t b) {
	uint16_t c = a + b;

	// can only happen due to overflow
	if (c < a) {
		c = -1;
	}
	return c;
}

void efektJezdiciBarvy() {
	for (uint16_t i = 0; i < led_strip_led_count; i++) {
		led_strip.setPixelColor(i, led_strip.ColorHSV(i * std::numeric_limits<uint16_t>::max() / led_strip_led_count, 255, 255));
		led_strip.show();
		delay(50);
	}
	for (uint16_t i = 0; i < led_strip_led_count; i++) {
		led_strip.setPixelColor(i, led_strip.ColorHSV(0, 0, 0));
		led_strip.show();
		delay(50);
	}
}

void nastavDuhu(uint16_t posun) {
	for (uint16_t i = 0; i < led_strip_led_count; i++) {
		led_strip.setPixelColor(i, led_strip.ColorHSV(i * std::numeric_limits<uint16_t>::max() / led_strip_led_count + posun, 255, 255));
	}
	led_strip.show();
}
void vypni() {
	for (uint16_t i = 0; i < led_strip_led_count; i++) {
		led_strip.setPixelColor(i, led_strip.ColorHSV(0, 0, 0));
	}
	led_strip.show();
}

void efektToceni() {
	for (uint16_t i = 0; i < std::numeric_limits<uint16_t>::max(); i += std::numeric_limits<uint16_t>::max() / 128) {
		nastavDuhu(i);
		delay(10);
	}
}

void efektBlikani() {
	for (uint16_t barva = 0; barva < std::numeric_limits<uint16_t>::max(); barva = saturatingAddU16(barva, std::numeric_limits<uint16_t>::max() / 8)) {
		for (uint16_t i = 0; i < led_strip_led_count; i++) {
			led_strip.setPixelColor(i, led_strip.ColorHSV(barva, 255, 255));
		}
		led_strip.show();
		delay(100);
		vypni();
		delay(200);
	}
}

float temperature = 0;
float relativeHumidity = 0;
uint8_t pumpSpeed = 0;

void onWebSocketEvent(AsyncWebSocket * server, AsyncWebSocketClient * client, AwsEventType type, void * arg, uint8_t *data, size_t len) {
	if (type == WS_EVT_CONNECT) {
		Serial.printf("ws[%s][%u] connect\n", server->url(), client->id());
	} else if (type == WS_EVT_DISCONNECT) {
		Serial.printf("ws[%s][%u] disconnect\n", server->url(), client->id());
	} else if (type == WS_EVT_ERROR) {
		Serial.printf("ws[%s][%u] error(%u): %s\n", server->url(), client->id(), *((uint16_t*)arg), (char*)data);
	} else if (type == WS_EVT_PONG) {
		Serial.printf("ws[%s][%u] pong[%u]: %s\n", server->url(), client->id(), len, (len)?(char*)data:"");
	} else if (type == WS_EVT_DATA) {
		AwsFrameInfo * info = (AwsFrameInfo*) arg;

		if (info->opcode == WS_TEXT) {
			client->text("text frames not supported!");
			return;
		}

		auto payload = std::vector<uint8_t>();
		// we expect command to be about this size
		payload.reserve(16);

		if (info->final && info->index == 0 && info->len == len) {
			//the whole message is in a single frame and we got all of it's data

			Serial.printf("ws[%s][%u] %s message (%llu bytes)\n", server->url(), client->id(), (info->opcode == WS_TEXT) ? "text" : "binary", info->len);

			payload.insert(payload.end(), &data[0], &data[0] + len);
		} else {
			//message is comprised of multiple frames or the frame is split into multiple packets

			if (info->index == 0) {
				if (info->num == 0) {
					Serial.printf("ws[%s][%u] %s-message start\n", server->url(), client->id(), (info->message_opcode == WS_TEXT) ? "text" : "binary");
				}
				Serial.printf("ws[%s][%u] frame[%u] start (%llu bytes)\n", server->url(), client->id(), info->num, info->len);
			}

			Serial.printf("ws[%s][%u] frame[%u] %s[%llu - %llu]: ", server->url(), client->id(), info->num, (info->message_opcode == WS_TEXT) ? "text" : "binary", info->index, info->index + len);

			payload.insert(payload.end(), &data[0], &data[0] + len);

			if ((info->index + len) == info->len) {
				Serial.printf("ws[%s][%u] frame[%u] end (%llu bytes)\n", server->url(), client->id(), info->num, info->len);
				if (info->final) {
					Serial.printf("ws[%s][%u] %s-message end\n", server->url(), client->id(), (info->message_opcode == WS_TEXT)?"text":"binary");
					client->text("acknowledged");
				}
			}
		}

		Serial.printf("Payload begin=%u end=%u: ", payload.begin(), payload.end());
		for (auto i : payload) Serial.printf("%u, ", i);
		Serial.println();

		auto command = postcard::parseClientCommand(payload.begin(), payload.end()).value();

		Serial.print("command decoded: ");
		Serial.println(command.index());

		if (auto* cmd = std::get_if<client_command::GetLedColour>(&command)) {
			uint8_t response[] = { 0x00, colour.red, colour.green, colour.blue };
			client->binary(response, sizeof(response) / sizeof(*response));
		} else if (auto* cmd = std::get_if<client_command::SetLedColour>(&command)) {
			colour = cmd->colour;
		} else if (auto* cmd = std::get_if<client_command::GetTemperature>(&command)) {
			// uint8_t response[] = { 0x00, temperature };
			// client->binary(response, sizeof(response) / sizeof(*response));
		} else if (auto* cmd = std::get_if<client_command::GetHumidity>(&command)) {

		} else if (auto* cmd = std::get_if<client_command::SetPumpSpeed>(&command)) {
			pumpSpeed = cmd->value;
		} else if (auto* cmd = std::get_if<client_command::SetLedMode>(&command)) {
			ledMode = cmd->mode;
			Serial.println(static_cast<uint8_t>(ledMode));
		}
	}
}

auto wiFiConnectTimeout = Countdown(20'000);

void setup() {
	Serial.begin(74880);
	while (!Serial) { delay(10); }

	pump.setup();
	dht.setup();
	led_strip.begin();
	led_strip.setBrightness(50);
	led_strip.show();

	// Set WiFi to station mode and disconnect from an AP if it was previously connected
	WiFi.mode(WIFI_STA);
	// WiFi.disconnect();
	WiFi.begin("3301-IoT", "mikrobus");
	wiFiConnectTimeout.start();

	webSocketHandler.onEvent(onWebSocketEvent);
	server.addHandler(&webSocketHandler);
	server.addHandler(&serverEvents);

	if (!LittleFS.begin()) {
		Serial.println("An error has occurred while mounting LittleFS");
		while (true) { delay(10'000); }
	}
	Serial.println("LittleFS mounted successfully");
	server.serveStatic("/", LittleFS, "/").setDefaultFile("index.html");

	AsyncElegantOTA.begin(&server);
	server.begin();
}

void loop() {
	static auto deltaCounter = DeltaCounter();
	static auto infoPrintTimer = Timer(5'000);

	auto delta = deltaCounter.delta_to(millis());

	webSocketHandler.cleanupClients();

	static auto waitForWiFiConnect = true;
	if (waitForWiFiConnect) {
		if (WiFi.status() == WL_CONNECTED) {
			waitForWiFiConnect = false;

			Serial.println("Successfully connected.");
			Serial.print("IP: ");
			Serial.println(WiFi.localIP());
		} else if (wiFiConnectTimeout.tick(delta)) {
			waitForWiFiConnect = false;

			Serial.println("Failed to connect.");
		} else {
			// wait
		}
	}

	pump.drive(Motor::Direction::forward, pumpSpeed);

	if (infoPrintTimer.tick(delta)) {
		temperature = dht.temperature();
		relativeHumidity = dht.relativeHumidity();
		Serial.print("Current temperature: ");
		Serial.println(temperature);
		Serial.print("Current relative humidity: ");
		Serial.println(relativeHumidity);
	}

	switch (ledMode) {
		case LedMode::SingleColourRainbow: {
			for (uint16_t i = 0; i < led_strip_led_count; i++) {
				led_strip.setPixelColor(i, led_strip.Color(colour.red, colour.green, colour.blue));
			}
			led_strip.show();
			break;
		} case LedMode::PartyHard: {
			efektBlikani();
			break;
		} case LedMode::SpinningRainbow: {
			efektJezdiciBarvy();
		} default: {
			break;
		}
	}

	delay(10);
}
