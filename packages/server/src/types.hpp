#pragma once

#include "stdint.h"

using instant_t = uint32_t;
using duration_t = uint32_t;
using pin_number_t = uint8_t;
