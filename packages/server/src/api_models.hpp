#pragma once

#include <variant>
#include <stdint.h>

namespace api_models {
	struct Colour {
		uint8_t red;
		uint8_t green;
		uint8_t blue;

		/*
		 * allow copy assigning `Colour` even if the target is volatile
		 *
		 * note that we don't return a reference to `this` from the operator.
		 * this is done to suppress GCC warning us about the volatile reference returned not being used.
		 * note that this disallows using assignments to `Colour` as expression
		 * (i.e.: `a = b = c` or `(a = b).method()`), but that's probably fine, since you probably don't want to do that with a volatile reference anyway.
		 */
		void operator=(Colour const& other) volatile {
			std::tie(
				this->red,
				this->green,
				this->blue
			) = std::tie(
				other.red,
				other.green,
				other.blue
			);
		}
	};

	enum class LedMode {
		SingleColourRainbow,
		PartyHard,
		SpinningRainbow
	};

	namespace client_command {
		struct GetLedColour {};
		struct SetLedColour { Colour colour; };
		struct GetTemperature {};
		struct GetHumidity {};
		struct SetLedBrightness { uint8_t value; };
		struct SetPumpSpeed { uint8_t value; };
		struct SetLedMode { LedMode mode; };
	}

	using ClientCommand = std::variant<
		client_command::GetLedColour,
		client_command::SetLedColour,
		client_command::GetTemperature,
		client_command::GetHumidity,
		client_command::SetLedBrightness,
		client_command::SetPumpSpeed,
		client_command::SetLedMode
	>;
}
