#include <ESP8266WiFi.h>
#include <Arduino.h>
#include <Adafruit_GFX.h>
#include <Adafruit_NeoPixel.h>

const uint16_t led_strip_led_count = 19;

auto led_strip = Adafruit_NeoPixel(led_strip_led_count, D10, NEO_GRB + NEO_KHZ400);

void setup() {
	Serial.begin(115200);

	led_strip.begin();
	led_strip.show();

	while (!Serial) { delay(1); }
}

void efektJezdiciBarvy() {
	for (uint16_t i = 0; i < led_strip_led_count; i++) {
		led_strip.setPixelColor(i, led_strip.ColorHSV(i * std::numeric_limits<uint16_t>::max() / led_strip_led_count, 255, 255));
		led_strip.show();
		delay(50);
	}
	for (uint16_t i = 0; i < led_strip_led_count; i++) {
		led_strip.setPixelColor(i, led_strip.ColorHSV(0, 0, 0));
		led_strip.show();
		delay(50);
	}
}

void nastavDuhu(uint16_t posun) {
	for (uint16_t i = 0; i < led_strip_led_count; i++) {
		led_strip.setPixelColor(i, led_strip.ColorHSV(i * std::numeric_limits<uint16_t>::max() / led_strip_led_count + posun, 255, 255));
	}
	led_strip.show();
}
void vypni() {
	for (uint16_t i = 0; i < led_strip_led_count; i++) {
		led_strip.setPixelColor(i, led_strip.ColorHSV(0, 0, 0));
	}
	led_strip.show();
}

void efektToceni() {
	for (uint16_t i = 0; i < std::numeric_limits<uint16_t>::max(); i += std::numeric_limits<uint16_t>::max() / 128) {
		nastavDuhu(i);
		delay(10);
	}
}

void efektBlikani() {
	for (uint16_t barva = 0; barva < std::numeric_limits<uint16_t>::max(); barva += std::numeric_limits<uint16_t>::max() / 8) {
		for (uint16_t i = 0; i < led_strip_led_count; i++) {
			led_strip.setPixelColor(i, led_strip.ColorHSV(barva, 255, 255));
		}
		led_strip.show();
		delay(500);
		vypni();
		delay(100);
	}
}

void loop() {
	// efektJezdiciBarvy();
	// efektToceni();
	efektBlikani();
}
