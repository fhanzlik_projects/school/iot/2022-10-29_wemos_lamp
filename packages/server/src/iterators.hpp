#pragma once

#include <array>
#include <iterator>

/// infinite iterator that yields elements from an array, wrapping when reaching the end
template<typename T, std::size_t N>
class ArrayLoopIterator: public std::iterator<
	std::input_iterator_tag, // iterator_category
	T,                       // value_type
	ptrdiff_t,               // difference_type
	const T*,                // pointer
	const T&                 // reference
>{
	private:
		std::array<T, N> items;
		std::size_t i = 0;

	public:
		explicit ArrayLoopIterator(std::array<T, N> items) : items(items) {}

		ArrayLoopIterator& operator++() {
			this->i = i == N-1 ? 0 : i + 1;
			return *this;
		}
		ArrayLoopIterator operator++(int) {
			auto retval = *this;
			++(*this);
			return retval;
		}
		bool operator==(ArrayLoopIterator other) const {
			return this->items == other->items && this->i == other->i;
		}
		bool operator!=(ArrayLoopIterator other) const {
			return !(*this == other);
		}
		T operator*() const {
			return items[i];
		}
};
