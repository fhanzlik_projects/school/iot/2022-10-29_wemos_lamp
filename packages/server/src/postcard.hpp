#pragma once

#include <optional>

#include "api_models.hpp"

namespace postcard {
	using std::optional;
	using std::nullopt;
	using api_models::ClientCommand;
	using api_models::LedMode;
	namespace client_command = api_models::client_command;

	template<typename Iterator> class Parser {
		public:
			Iterator current;
			Iterator end;

			explicit Parser(Iterator begin, Iterator end) : current(begin), end(end) {};

			optional<uint8_t> eatU8() {
				if (this->current == this->end) return nullopt;
				return optional(*this->current++);
			}
			optional<uint32_t> eatVarLenU32() {
				uint32_t result = 0;
				uint8_t i = 0;

				while (true) {
					auto byte = this->eatU8();
					if (byte == nullopt) return nullopt;
					auto value     = *byte & 0b0111'1111;
					auto continued = *byte & 0b1000'0000;

					result |= value << i;
					i += 7;

					if (!continued) break;
				};

				return result;
			}
	};

	template <typename Iterator> optional<ClientCommand> parseClientCommand(Iterator begin, Iterator end) {
		auto parser = Parser(begin, end);

		enum ClientCommandType {
			GetLedColour,
			SetLedColour,
			GetTemperature,
			GetHumidity,
			SetLedBrightness,
			SetPumpSpeed,
			SetLedMode
		};

		switch (parser.eatVarLenU32().value()) {
			case GetLedColour: {
				return client_command::GetLedColour {};
			} case SetLedColour: {
				auto red   = parser.eatU8().value();
				auto green = parser.eatU8().value();
				auto blue  = parser.eatU8().value();

				return client_command::SetLedColour { .colour = { .red = red, .green = green, .blue = blue } };
			} case GetTemperature: {
				return client_command::GetTemperature {};
			} case GetHumidity: {
				return client_command::GetHumidity {};
			} case SetLedBrightness: {
				return client_command::SetLedBrightness { .value = parser.eatU8().value() };
			} case SetPumpSpeed: {
				return client_command::SetPumpSpeed { .value = parser.eatU8().value() };
			} case SetLedMode: {
				LedMode mode;
				auto id = parser.eatVarLenU32().value();
				switch (id) {
					case 0: mode = LedMode::SingleColourRainbow; break;
					case 1: mode = LedMode::PartyHard; break;
					case 2: mode = LedMode::SpinningRainbow; break;
					default: break;
				}
				return client_command::SetLedMode { .mode = mode };
			} default: {
				return nullopt;
			}
		}
	}

	enum ServerResponseType {
		GetLedColour
	};
}
