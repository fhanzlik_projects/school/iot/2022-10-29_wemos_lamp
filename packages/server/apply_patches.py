from os.path import join, isfile

Import("env")

LIBDEPS_DIR = env['PROJECT_LIBDEPS_DIR']
PACKAGE_DIR = join(LIBDEPS_DIR, "d1_mini", "ESP Async WebServer")
patchflag_path = join(PACKAGE_DIR, ".patching-done")

# patch file only if we didn't do it before
if not isfile(join(PACKAGE_DIR, ".patching-done")):
    original_file = join(PACKAGE_DIR, "src", "WebResponses.cpp")
    patched_file = join("patches", "00-ESPAsyncWebServer-WebResponses.cpp.patch")

    assert isfile(original_file) and isfile(patched_file)

    env.Execute("patch '%s' '%s'" % (original_file, patched_file))

    def _touch(path):
        with open(path, "w") as fp:
            fp.write("")

    env.Execute(lambda *args, **kwargs: _touch(patchflag_path))
