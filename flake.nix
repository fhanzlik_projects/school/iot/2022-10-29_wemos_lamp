{
	description = "recourse";

	inputs = {
		flake-compat = { url = "github:edolstra/flake-compat"; flake = false; };
		nixpkgs      = { url = "github:nixos/nixpkgs/nixos-unstable";         };
		flake-utils  = { url = "github:numtide/flake-utils";                  };
		rust-overlay = { url = "github:oxalica/rust-overlay"; inputs = { nixpkgs.follows = "nixpkgs"; flake-utils.follows = "flake-utils"; }; };
	};

	outputs = { self, nixpkgs, flake-utils, rust-overlay, ... }:
		flake-utils.lib.eachDefaultSystem (system:
			let
				pkgs = import nixpkgs {
					inherit system;
					overlays = [
						rust-overlay.overlays.default
						(final: prev: {
							perseus-cli = final.callPackage ./nix/packages/perseus-cli.nix { CoreServices = null; };
						})
					];
				};

				rust = import ./nix/rust.nix {
					inherit pkgs;
					channel = "nightly";
					version = "2022-11-09";
				};
			in rec {
				devShells.default = pkgs.mkShell rec {
					nativeBuildInputs = with pkgs; [
						python3
						platformio
						avrdude

						perseus-cli
						rust.toolchain
					];
				};
			}
		);
}
